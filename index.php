<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>Carga de Anexos</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
	<div class="container-fluid bg-faded">
		<div class="row justify-content-center">
			<div class="col-md-6">
				<?php if(isset($imagen)){?>
				<div class="alert alert-info alert-dismissable">
					<h4>Datos Guardados</h4>
				</div>
				<?php }?>
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Formulario Con Anexos</h4>
					</div>
					<div class="card-body">
						<form method="POST" action="prosesar.php" enctype="multipart/form-data">
							<div class="form-row">
								<div class="col">
									<label for="nombre">Nombre</label>
									<input name="nombre" type="text" class="form-control" value="<?php (isset($nombre)) ? print($nombre) : ''?>">
								</div>
							</div>
							<div class="form-row">
								<div class="col">
									<label for="apellido">Apellido</label>
									<input name="apellido" type="text" class="form-control" value="<?php (isset($apellido)) ? print($apellido) : ''?>">
								</div>
							</div>
							<div class="form-row">
								<div class="col">
									<label>Foto</label>
									<?php if(isset($imagen)){?>
									<img class="img-thumbnail" src='<?php (isset($imagen)) ? print("anexos/$imagen"): "" ?>'>
									<?php }else{?>
									<input type="file" accept="image/png, image/jpeg, .pdf" name="foto" style="padding: 6px 12px; border:1px solid #ccc; border-radius: 6px; width: 100%;">
									<hr>
									<?php }?>
								</div>								
							</div>
							<hr>
							<div class="form-row">
								<div class="col text-right">
									<?php if(isset($imagen)){?>
										<button type="button" class="btn btn-danger" onclick="window.location='index.php'">Regresar</button>
									<?php }else {?>
										<button class="btn btn-primary">Guardar</button>
										<button type="reset" class="btn btn-danger">Cancelar</button>
									<?php }?>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="js/jquery-3.3.1.slim.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>